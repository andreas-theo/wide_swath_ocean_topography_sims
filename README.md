# InterferometricParameterSims

This project holds functions that set up and run simulations of satellite SAR formations. In particular, simulations that propagate orbits, compute the scene geometry and project the region of support of SAR observations on the surface are included.
