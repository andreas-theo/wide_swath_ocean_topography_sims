### A Pluto.jl notebook ###
# v0.19.41

#> [frontmatter]
#> title = "Interferometric sensitivity and temporal lag"
#> date = "2023-11-07"
#> tags = ["synthetic-aperture radar", "insar"]
#> description = "A spectral view"
#> 
#>     [[frontmatter.author]]
#>     name = "Andreas Theodosiou"

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ d6aaf3d0-c097-4995-8dbd-5ae1d1842dc6
begin
    import Pkg
    # activate the shared project environment
    Pkg.activate(Base.current_project())
    # instantiate, i.e. make sure that all packages are downloaded
    Pkg.instantiate()

	using Statistics: quantile
    using Plots, LinearAlgebra, Revise, Plots
	using Dates: today
	using PlutoUI: CheckBox, TableOfContents
	using InterferometricParameterSims
	import SatelliteToolbox, SARToolbox
end

# ╔═╡ f8e8b47d-703e-421b-bc99-6d71ed269b81
TableOfContents()

# ╔═╡ 664d8a9e-d440-4c9c-9780-b6a9dd15580e
md"# Set up
Set up the font for the plots"

# ╔═╡ 978e10cc-700b-4d26-9d18-5417e77f5361
begin
	plot_font = "Computer Modern"
	default_dpi = 100
	dpi = 300
	resetfontsizes()
	scalefontsizes(0.5)
	default(fontfamily=plot_font, size=(3.5*default_dpi, 3*default_dpi), dpi=dpi) #titlefontsize=11, labelfontsize=9, colorbar_titlefontsize=9)
	# PythonPlot.matplotlib.rc("text", usetex=true)
	# PythonPlot.matplotlib.rc("mathtext", fontset="cm")
	# PythonPlot.matplotlib.rc("font", family="serif", serif="cmr10")
	# PythonPlot.matplotlib.rc("axes.formatter", use_mathtext = true)
end

# ╔═╡ 22c5dbeb-ce81-4924-8296-d2e1c1aa260d
md"""Set up the bistatic simulation parameters."""

# ╔═╡ cb2e0153-f136-4f44-9442-13634e26419c
begin
	cd(dirname(Base.current_project()))
	parameters = InterferometricParameterSims.bistatic_setup(deg2rad.(range(26, 41, step=1)), range(-3E6, 3E6, step=0.5E6), 1, 1, 1E-3)
end

# ╔═╡ 704658e6-df87-4ba0-babf-2092acec7e88
begin
	save_figs_checkbox = @bind save_figures CheckBox(default=false)
	
	md"""
	Save the figures? $(save_figs_checkbox)
	"""
end

# ╔═╡ 7f0ff276-42ec-496b-9082-bc040311bf86
begin
	if save_figures
		plots_dir = joinpath(homedir(), "Code/InterferometricParameterSims/assets", "results_" * string(today()))
		mkpath(plots_dir)

		md"""
		Figures will be saved in $plots_dir
		"""

	else
		md"""
		Figures will not be saved.
		"""
	end
end

# ╔═╡ 227757bb-bc29-4f4a-9fe1-8df697986bb0
md"""Let's initialise the orbit propagators and propagate for one orbital period."""

# ╔═╡ 4d94a6ce-8201-438f-a3f9-053733bd8eef
begin
	inc_range = 30:45
	orbp = SARToolbox.create_reference_orbit("assets/sentinel-1B")
	T = SatelliteToolbox.orbital_period(SatelliteToolbox.Propagators.mean_elements(orbp))
	time_vec = 0:1:T
	orbp_h1 = SatelliteToolbox.Propagators.init(Val(:J2), SatelliteToolbox.Propagators.mean_elements(orbp) + parameters.ref_companion)
	asc_t = SARToolbox.ascend_descend(orbp, 0.001)
	o_h1, r_h1, v_h1 = SARToolbox.propagate_with_mean_elements!(orbp_h1, asc_t:asc_t+T)
	epoch = getfield.(o_h1, :t)
	svs = InterferometricParameterSims.convert_to_ecef.(epoch, r_h1, v_h1)
	v_h1_e = getfield.(svs, :v)
	v_norm_e = norm.(v_h1_e)
end

# ╔═╡ c66e73e0-86b3-4bb7-be01-830927a85802
md"""
# Temporal Lag
"""

# ╔═╡ 5cb26618-40e8-4b4f-9b62-2cfc19f4b1bb
md"""## Helix formation
Simulate a helix formation with both an $a \Delta e$ and an $a \Delta \Omega$.
### Analytic temporal lag
"""

# ╔═╡ 512d0b42-a67a-4258-abf5-790143cb4efd
begin
	res = InterferometricParameterSims.run_parallel_temporal_shift_rel_analytical_p(parameters)
	res_t = getindex.(res, 1)
end

# ╔═╡ 768ccbb2-cccc-44ee-a282-24d9a7953504
md"The maximum analytical temporal lag is $(round(maximum(res_t)*1000, digits=3)) ms. The minimum is $(round(minimum(res_t)*1000, digits=3)) ms."

# ╔═╡ 7f5a6772-c6f5-4d21-b23f-ff4226f762d9
p_t_analytical = contourf(inc_range, time_vec/60, res_t*1000, lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Analytic temporal lag", colorbar_title="\nTemporal lag /ms", c=cgrad(:broc, 7/10), framestyle=:box, levels=-7:1:3, clims=(-7, 3))

# ╔═╡ 7aa92890-0a75-4a5b-9806-21cb53e75d5a
md"""### Geometric temporal lag
Now we will compute the temporal lag using the geometric method and compare the difference.
"""

# ╔═╡ 287976e0-c9bb-4391-934b-6fd2c7fb8dc2
parameters_geo = InterferometricParameterSims.bistatic_setup_geo(deg2rad.(range(26, 41, step=1)), range(-3E6, 3E6, step=0.5E6), 1, 1, 1E-3)

# ╔═╡ ec22824d-1b88-48c4-857e-4dc8341e4b63
t_geometric, _, _, _ = InterferometricParameterSims.run_temporal_shift_geometric(parameters)

# ╔═╡ a3597ce5-c699-4f91-93d4-6fcb991da4aa
md"The maximum geometric temporal lag is $(round(maximum(t_geometric) * 1000, digits=2)) ms. The minimum is $(round(minimum(t_geometric) * 1000, digits=2)) ms."

# ╔═╡ 22b2d51e-b7c0-41b5-a61a-12387c46a3ce
p_t_geometric = contourf(inc_range, time_vec/60, t_geometric * 1000, lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Geometric temporal lag", colorbar_title="\nTemporal lag /ms", c=cgrad(:broc, 7/10), levels=-7:1:3, clims=(-7, 3), framestyle=:box)

# ╔═╡ 57f1277a-928d-494d-94c0-a54170014bc2
p_helix_analytic_and_geo = plot(p_t_analytical, p_t_geometric, layout=(1,2))

# ╔═╡ 2060f79e-2948-4392-8d21-2ccac319053e
if save_figures
	savefig(p_helix_analytic_and_geo, joinpath(plots_dir, "temporal_analytic_and_geo.svg"))
end

# ╔═╡ 7c32aa98-2a46-4d1e-ae83-015204e05e4a
contourf((res_t - t_geometric) ./ (abs.(res_t) + abs.(t_geometric)) * 2, lw=0, c=:broc, levels=-2:0.4:2)

# ╔═╡ a6853612-81e6-40ee-9801-d00f03841725
abs_error_temporal = abs.(res_t - t_geometric)

# ╔═╡ 964ed01f-f535-4305-b399-de1bfb943707
md"The maximum absolute difference is $(round(maximum(abs_error_temporal)*1000, digits=3)) ms."

# ╔═╡ 68ca9815-d9b9-40c0-862a-dc1c4b4f6a85
p_temporal_abs_error = contourf(inc_range, time_vec/60, abs_error_temporal*1E3, lw=0, c=:batlow, levels=0:0.05:0.4, clims=(0, 0.4), xlabel="Incident angle /°", title="Temporal lag absolute error", colorbar_title="\nAbsolute error /ms", framestyle=:box)

# ╔═╡ 9f045fc0-684c-44e0-b9ed-e3802a21b325
p_temporal_analytic_and_error = plot(p_t_analytical, p_temporal_abs_error, layout=(1,2), link=:y)

# ╔═╡ ba4e88da-a53c-471f-b790-13005f288b4f
if save_figures
	savefig(p_temporal_analytic_and_error, joinpath(plots_dir, "temporal_analytic_and_error.svg"))
end

# ╔═╡ 2cc467a1-0d8c-4396-acdd-98bbeb7b49fe
md"""
### Geometric method - Placing the ME at the midpoint between TX and RX
"""

# ╔═╡ 68ce6113-a834-42fd-b8fc-9df04af5ea39
at_baseline_helix, _, _ = InterferometricParameterSims.run_temporal_shift_drama_method(parameters)

# ╔═╡ 6771d8ec-1196-496f-9705-8c4fecf0ccac
md"The maximum geometric temporal lag after placing the ME at the midpoint is $(round(maximum(at_baseline_helix ./ v_norm_e)*1000/2, digits=2)) ms. The minimum is $(round(minimum(at_baseline_helix ./ v_norm_e)*1000/2, digits=2)) ms."

# ╔═╡ 87ae022d-fbd5-481e-a3ca-f0bbb73a7384
p_t_geometric_drama = contourf(inc_range, time_vec/60, at_baseline_helix * 1000 /2 ./ v_norm_e, lw=0, xlabel="Incident angle /°", title="Geometric temporal lag", colorbar_title="\nTemporal lag /ms", c=cgrad(:broc, 8/11), clims=(-13,7), levels=-13:2:7, framestyle=:box)

# ╔═╡ c6e04f62-8ea5-4e46-b5f2-6b8465507437
p_helix_analytic_and_geo_drama = plot(p_t_analytical, p_t_geometric_drama, layout=(1,2))

# ╔═╡ edc290e5-e115-4e68-858d-e3dcdbf0c40b
contourf(abs.(res_t -  at_baseline_helix /2 ./ v_norm_e) * 1000, c=:batlow, clims=(0, 7), levels=0:7, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Temporal lag ansolute error", colorbar_title="\nAbsolute error /ms")

# ╔═╡ 5ab28385-8563-45d1-b5eb-77db3b446e79
md"""### Geometric temporal lag surface projection"""

# ╔═╡ 2dadea49-f53b-4e23-a070-6b9efea79ceb
t_geometric_surface_projection, _, _ = InterferometricParameterSims.run_temporal_shift_drama_method_surface(parameters_geo)

# ╔═╡ 706343a8-793b-433e-8e7b-335b53b4967f
md"The maximum geometric temporal lag after projecting on the surface is $(round(maximum(t_geometric_surface_projection ./ v_norm_e)*1000/2, digits=2)) ms. The minimum is $(round(minimum(t_geometric_surface_projection ./ v_norm_e)*1000/2, digits=2)) ms."

# ╔═╡ 33ed325b-c4d6-4777-98ba-1c153b9c1a0f
p_t_geometric_surface = contourf(inc_range, time_vec/60, t_geometric_surface_projection * 1000 / 2 ./ v_norm_e, lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Geometric temporal lag", colorbar_title="\nTemporal lag /ms", c=cgrad(:broc, 10/12), clims=(-10,2), levels=-10:1:2, framestyle=:box)

# ╔═╡ abee8f7b-da5b-49e8-9218-599f5f36d2b2
md"""## Convoy formation
Simulate a formation where the two spacecraft share the same orbital plane and only have an along-track separation.
### Analytic temporal lag
Calculate the temporal lag analytically, assuming bistatic operation. This should give a temporal lag that corresponds to half the physical along-track separation
"""

# ╔═╡ 583edd33-eb24-48e4-ae5f-b48cf9a6184b
parameters_mono =  InterferometricParameterSims.monostatic_setup(deg2rad.(range(26, 41, step=1)), range(-3E6, 3E6, step=0.5E6), 1, 1, 1E-3)

# ╔═╡ b1339794-cf6a-4216-a5b5-30f49466e39b
begin
	res_bi = InterferometricParameterSims.run_parallel_temporal_shift_rel_analytical_p(parameters_mono)
	res_bi_t = getindex.(res_bi, 1)
end

# ╔═╡ 16ef7591-e60c-4410-817d-601caf63d918
begin
	at_baseline_bi = res_bi_t .* v_norm_e
	p_t_bi_analytical = contourf(inc_range, time_vec/60, at_baseline_bi, lw=0, xlabel="Incident angle /°", title="Bistatic", colorbar_title="\nAlong-track baseline /m", c=:broc, clims=(minimum(round.(at_baseline_bi)), maximum(round.(at_baseline_bi))), levels=9, framestyle=:box)
end

# ╔═╡ ee737cfe-22a7-4cce-9f20-1d3ded3e9f00
begin
	res_mono = InterferometricParameterSims.run_parallel_temporal_shift_rel_analytical_p(parameters_mono, true)
	res_mono_t = getindex.(res_mono, 1)
end

# ╔═╡ e107fe9e-bf7c-49e1-8963-02461008ab95
begin
	at_baseline_mono = res_mono_t .* v_norm_e
	p_t_mono_analytical = contourf(inc_range, time_vec/60, at_baseline_mono, lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Monostatic", colorbar_title="\nAlong-track baseline /m", c=:broc, clims=(minimum(round.(at_baseline_mono)), maximum(round.(at_baseline_mono))), levels=9, framestyle=:box)
end

# ╔═╡ b1305eb4-047a-4b43-99b5-7384a6427658
md"""### Geometric temporal lag"""

# ╔═╡ 28e0a792-62de-4d3b-92b1-51be58373b67
at_mono_geometric, _, _ = InterferometricParameterSims.run_temporal_shift_drama_method(parameters_mono)

# ╔═╡ 3f545581-bfed-4852-a3e9-105b97c73662
p_t_mono_geometric = contourf(inc_range, time_vec/60, at_mono_geometric, lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Monostatic", colorbar_title="\nAlong-track baseline /m", c=:broc, clims=(minimum(round.(at_mono_geometric)), maximum(round.(at_mono_geometric))), levels=9, framestyle=:box)

# ╔═╡ de6c8a42-62d6-4b94-bccc-8c4758c2fee3
p_temporal_only = plot(p_t_mono_analytical, p_t_bi_analytical, layout=(1,2), link=:y)

# ╔═╡ 333c4a63-d572-4520-a156-e513e2ce1ebc
if save_figures
	savefig(p_temporal_only, joinpath(plots_dir, "t_lag_flat_mono_and_bi.svg"))
end

# ╔═╡ 12e5834c-ff04-4d4a-8d5d-92dd9f38595b
md"""### Geometric temporal lag - Surface projection"""

# ╔═╡ be9d7df8-9a3d-44e6-a09e-50a564932f32
t_mono_geometric_surface_projection, _, _ = InterferometricParameterSims.run_temporal_shift_drama_method_surface(parameters_mono)

# ╔═╡ ae7c596c-b9a2-4b17-bd6e-b25cdd92d09b
p_t_mono_geometric_surface = contourf(inc_range, time_vec/60, t_mono_geometric_surface_projection, lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Geometric temporal lag - Surface projection", colorbar_title="\nAlong-track baseline /m", c=:batlow, clims=(minimum(round.(t_mono_geometric_surface_projection)), maximum(round.(t_mono_geometric_surface_projection))), levels=9, dpi=300, colorbar_titlefontsize=8, right_margin=3Plots.mm, framestyle=:box, left_margin=5Plots.mm)

# ╔═╡ d4adf3b2-3ad1-4c92-8f62-90190cfd3cb1
delta_t = abs.(at_baseline_mono - at_mono_geometric/2) ./ v_norm_e

# ╔═╡ ced822e9-25c1-494d-8cce-7c75739118d9
contourf(delta_t * 1000, c=:batlow)

# ╔═╡ 164d4be4-fa4b-41e1-b313-634b2f396a6f
md"## Industry formation

The industry consortium came up with a formation of $$a \Delta e = 750$$ m, $$a \Delta \Omega = 2100$$ m. Let's see how the methods compare with this formation."

# ╔═╡ 3cad3068-49b9-4c9a-ab24-83742c74496a
parameters_industry = InterferometricParameterSims.bistatic_setup_industry(deg2rad.(range(26, 41, step=1)), range(-3E6, 3E6, step=0.5E6), 1, 1, 1E-3)

# ╔═╡ cb396a3c-3ddf-4a82-92e8-d8641bdaed55
md"""
### Analytic method
"""

# ╔═╡ a5fa6096-b9ca-43a7-aaca-fd25b7966b35
begin
	res_industry = InterferometricParameterSims.run_parallel_temporal_shift_rel_analytical_p(parameters_industry)
	res_industry_t = getindex.(res_industry, 1)
end

# ╔═╡ d80314b7-5340-4ee1-bd7c-145813b4fd3d
md"The maximum analytical temporal lag of the thid formation is $(round(maximum(res_industry_t)*1000, digits=2)) ms. The minimum is $(round(minimum(res_industry_t)*1000, digits=2)) ms."

# ╔═╡ 18a69d19-05bf-48b7-bb23-6c1c6280410d
p_t_industry_analytical = contourf(inc_range, time_vec/60, clamp.(res_industry_t*1000, -55, 35), lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Analytic temporal lag", colorbar_title="\nTemporal lag /ms", c=cgrad(:broc, 55/90), levels=-55:5:35, clims=(-55, 35), framestyle=:box)

# ╔═╡ bb942269-c415-46d5-a179-82f4576b5c23
md"""
### Geometric method - ME at midpoint
"""

# ╔═╡ aa1e39e5-ff48-4718-9820-016ba3c1424b
t_industry_geometric, _, _ = InterferometricParameterSims.run_temporal_shift_drama_method(parameters_industry)

# ╔═╡ 5ac13f63-065d-4e4d-a4e2-ffc10e2f2ad5
md"The maximum geometric temporal lag of this formation is $(round(maximum(t_industry_geometric./ v_norm_e)*1000/2, digits=2)) ms. The minimum is $(round(minimum(t_industry_geometric ./ v_norm_e)*1000/2, digits=2)) ms."

# ╔═╡ bfb5fa01-2a9b-4941-a924-81e793af8e68
p_t_industry_geometric = contourf(inc_range, time_vec/60, t_industry_geometric * 1000 / 2 ./ v_norm_e, lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Geometric temporal lag", colorbar_title="\nTemporal lag /ms", c=cgrad(:broc, 75/130), dpi=300, colorbar_titlefontsize=8, right_margin=3Plots.mm, clims=(-75,55), levels=-75:10:55, framestyle=:box, left_margin=5Plots.mm)

# ╔═╡ ec952303-270a-44e2-9b3c-93eb1b1d136c
md"# Sensitivity
Here we're calculating the sensitivity to height using the spectral and geometric methods and comparing the results.

## Formation with only normal separation
This is a formation with only an $a \Delta \Omega$ parameter.
"

# ╔═╡ 35034430-d6a1-4e61-9cc9-9bb2d2769134
parameters_only_normal = InterferometricParameterSims.monostatic_setup_only_normal_separation(deg2rad.(range(26, 41, step=1)), range(-3E6, 3E6, step=0.5E6), 1, 1, 1E-3)

# ╔═╡ 1d9e2aed-3ba6-4a97-97b6-05ecf0e9af04
md"""
### Spectral method
"""

# ╔═╡ b72a1a57-377a-4ca6-bbe6-f7a474d60fb0
sensitivity_sp_normal_only = InterferometricParameterSims.run_parallel_sensitivity_analytical_p(parameters_only_normal, true)

# ╔═╡ a5e75057-13b5-40af-a09e-4c4fc7050026
maximum(abs.(sensitivity_sp_normal_only))

# ╔═╡ f6c97db0-6622-4dc9-8522-b210776a0284
p_sensitivity_sp_normal_only = contourf(inc_range, time_vec/60, abs.(sensitivity_sp_normal_only), lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Sensitivity to height", colorbar_title="\nSensitivity /rad/m", c=:batlow, clims=(0, 0.36), levels=0:.04:0.36, framestyle=:box,)

# ╔═╡ 2cd870d6-5767-498d-ad72-f5573d4b7fae
md"""
### Geometric method
"""

# ╔═╡ 0f584729-54e4-4321-8919-a69cdf1e8018
# _, _, sensitivity_drama_normal_only, _ = InterferometricParameterSims.run_temporal_shift_geometric(parameters_only_normal)
_, sensitivity_drama_normal_only, _ = InterferometricParameterSims.run_temporal_shift_drama_method(parameters_only_normal)

# ╔═╡ 5c42babb-71e9-46f8-81ce-7fd538e9b9ea
contourf(inc_range, time_vec/60, abs.(sensitivity_drama_normal_only), lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Sensitivity to height", colorbar_title="\nSensitivity /rad/m", c=:batlow, clims=(0, 0.36), levels=0:.04:0.36, framestyle=:box)

# ╔═╡ c4b2af99-8525-422f-82ec-aba9f354d280
begin
	Δsensitivity_normal_only = abs.(abs.(sensitivity_sp_normal_only) .- abs.(sensitivity_drama_normal_only))
	Δsensitivity_rel_normal_only = Δsensitivity_normal_only ./ abs.(sensitivity_sp_normal_only)
end

# ╔═╡ ab477815-27b2-47d3-a948-46a62e10c81e
contourf(Δsensitivity_normal_only, lw=0, c=:batlow, levels=5)

# ╔═╡ 2627112c-e834-4288-b28b-b74383ae9bf6
maximum(Δsensitivity_rel_normal_only*100)

# ╔═╡ 4761b4b6-1e82-4acf-8ebf-81036f5bd794
p_Δsensitivity_rel_normal_only = contourf(inc_range, time_vec/60, Δsensitivity_rel_normal_only*100, lw=0, xlabel="Incident angle /°", title="Sensitivity relative error", colorbar_title="\nRelative error /%", c=:batlow, clims=(0, 0.45), levels=0:.05:0.45, framestyle=:box)

# ╔═╡ fc9db39e-a4cc-4292-a407-29193f7add64
contourf(abs.(sensitivity_sp_normal_only), c=:batlow)

# ╔═╡ 7323b8a4-6680-4c20-b2f7-844f8270e62d
contourf(abs.(sensitivity_drama_normal_only), c=:batlow)

# ╔═╡ 55cd80c8-a3f6-4ab3-9b5c-9b24bf97a04e
p_sensitivity_and_error_only_normal = plot(p_sensitivity_sp_normal_only, p_Δsensitivity_rel_normal_only, layout=(1,2))

# ╔═╡ f1338d3b-9f0c-4d83-9ec1-fb630a3cf2fa
md"""
## Flat-Earth simulation

Let's carry out the same simulation as before but in a flat-Earth frame, neglecting the effects of the Earth's rotation and curvature.
"""

# ╔═╡ fb1b8c7e-076e-49f9-9c49-7ee0ee3b851a
begin
	sensitivity_flat = InterferometricParameterSims.run_parallel_temporal_shift_rel_flat_analytical(parameters_only_normal)
	sensitivity_flat_sp = getfield.(sensitivity_flat, 1)
	sensitivity_flat_cl = getfield.(sensitivity_flat, 2)
end

# ╔═╡ 1a24eaa7-a26f-4076-b443-5511176d30bc
maximum(sensitivity_flat_cl)

# ╔═╡ d7da8b59-295b-4abb-9bb9-fbe1d14fd798
p_sensitivity_flat_sp_normal_only = contourf(inc_range, time_vec/60, abs.(sensitivity_flat_sp), lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Sensitivity to height", colorbar_title="\nSensitivity /rad/m", c=:batlow, clims=(0, 0.4), levels=0:.05:0.4, framestyle=:box)

# ╔═╡ 2d5f3334-a045-4985-891b-3e64dff76520
rel_sens_flat_error = abs.(abs.(sensitivity_flat_sp) - abs.(sensitivity_flat_cl)) ./ abs.(sensitivity_flat_sp)

# ╔═╡ 9eca0cbe-1f5a-44c8-a444-a1bd4fa23baa
p_Δsensitivity_rel_flat_normal_only = contourf(inc_range, time_vec/60, rel_sens_flat_error*100, lw=0, xlabel="Incident angle /°", title="Sensitivity relative error", colorbar_title="\nRelative error /%", c=:batlow, clims=(0, 0.11), levels=0:.01:0.11, framestyle=:box)

# ╔═╡ ed495580-6719-45f6-9806-3d4f8ecec597
p_sensitivity_flat_and_error_only_normal = plot(p_sensitivity_flat_sp_normal_only, p_Δsensitivity_rel_flat_normal_only, layout=(1,2))

# ╔═╡ 6158312a-9f1f-4cee-8caf-97701ef70829
if save_figures
	savefig(p_sensitivity_flat_and_error_only_normal, joinpath(plots_dir, "sensitivity_flat_and_error.svg"))
end

# ╔═╡ ad685b43-4971-4a9f-b4cf-4010a0787794
md"""
## Helix formation
### Spectral method
"""

# ╔═╡ a76ab46b-737b-4c61-af07-3bd038b9c8b3
sensitivity_sp = InterferometricParameterSims.run_parallel_sensitivity_analytical_p(parameters)

# ╔═╡ 321315f9-ca3b-4829-b441-6734284c088a
md"The maximum analytical sensitivity is $(round(maximum(sensitivity_sp), digits=4)) rad/m. The minimum is $(round(minimum(sensitivity_sp), digits=4)) rad/m."

# ╔═╡ 294bf5d3-3aba-46b2-863b-95cf5c0e2e1d
p_sensitivity_sp = contourf(inc_range, time_vec/60, abs.(sensitivity_sp), lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Sensitivity to height", colorbar_title="\nSensitivity /rad/m", c=:batlow, clims=(0, 0.18), levels=0:.02:0.18, framestyle=:box)

# ╔═╡ 8c86a59c-f7b3-45ac-8829-5847595941f4
if save_figures
	savefig(p_sensitivity_sp, joinpath(plots_dir, "sensitivity.svg"))
end

# ╔═╡ abe7d587-389c-44bd-82f3-daa768a1a782
md"""
### Geometric method
"""

# ╔═╡ 14fe0e89-ec6e-4307-a1e0-28938ce6c4ef
_, _, sensitivity_geometric, b_perp_geometric = InterferometricParameterSims.run_temporal_shift_geometric(parameters, conventional_inc=true, los_scaling=true)

# ╔═╡ 093ace7d-e5ef-47ed-aaa1-c873c840f809
begin
	Δsensitivity = abs.(abs.(sensitivity_sp) .- abs.(sensitivity_geometric))
	Δsensitivity_rel = Δsensitivity ./ abs.(sensitivity_sp)
end

# ╔═╡ bcd1c3e2-51df-4056-8b8b-d96bde7e6fc7
contourf(inc_range, time_vec/60, abs.(sensitivity_geometric), lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Sensitivity to height", colorbar_title="\nSensitivity /rad/m", c=:batlow, clims=(0, 0.18), levels=0:.02:0.18, framestyle=:box)

# ╔═╡ 6d36b020-d049-4aa4-9024-bfa4000675ca
md"""
The maximum relative error in the region where the orbits do not cross is $(round(maximum(Δsensitivity_rel[270:2900, :])*100; digits=2))%.
"""

# ╔═╡ 34245985-4d17-432c-b780-b044d2104e59
md"""
The 2nd percentile of the relative error is $(round(quantile(vec(Δsensitivity_rel*100), 0.02); digits=2))% and the 98th percentile is $(round(quantile(vec(Δsensitivity_rel*100), 0.98); digits=2))%.
"""

# ╔═╡ 04958ba0-a1a3-463b-b402-b2cdda88aa46
begin
	p_Δsensitivity_rel = contourf(inc_range, time_vec/60, Δsensitivity_rel*100, lw=0, xlabel="Incident angle /°", title="Sensitivity relative error", colorbar_title="\nRelative error /%", c=:batlow, clims=(0, 7), levels=0:7, framestyle=:box)
end

# ╔═╡ 5afba60e-3649-495d-9381-21cf1ce3e214
md"""
Here we're checking the sensitivity over the region where there is no crossing of the satellites. This excludes the parts where the sensitivty tends to zero so we can more easily check the maximum relative error.
"""

# ╔═╡ 0cdd26b8-9531-4f27-b419-dc4a93bf701c
contourf(Δsensitivity_rel[270:2900, :]*100, lw=0, xlabel="Incident angle /°", title="Sensitivity relative error", colorbar_title="\nRelative error /%", c=:batlow, clims=(0, 5), levels=0:0.5:5, framestyle=:box)

# ╔═╡ 3d17259b-7106-4f93-a63c-0bbd524a0d60
p_sensitivity_and_error = plot(p_sensitivity_sp, p_Δsensitivity_rel, layout=(1,2))

# ╔═╡ 0449acd8-20e9-4963-80da-2edc8d11c845
if save_figures
	savefig(p_sensitivity_and_error, joinpath(plots_dir, "sensitivity_and_error.svg"))
end

# ╔═╡ 7295121b-32dc-406a-92ae-708598152f31
md"""
### Geometric method using alternative incidence angle and scaling
Now let's compute the geometric sensitivity using a calibrated incidence angle to get the best possible ageement with the spectral sensitivity.
"""

# ╔═╡ 0ee91179-93ec-4e83-bcf7-d8e6565b618e
_, _, sensitivity_geometric_cal_inc, b_perp_geom_cal = InterferometricParameterSims.run_temporal_shift_geometric(parameters; conventional_inc=false, los_scaling=true)

# ╔═╡ b490bb28-209d-4865-9294-6e5b53140f56
begin
	Δsensitivity_cal = abs.(abs.(sensitivity_sp) .- abs.(sensitivity_geometric_cal_inc))
	Δsensitivity_cal_rel = Δsensitivity_cal ./ abs.(sensitivity_sp)
end

# ╔═╡ d53dd8f5-0b58-42dc-a3d8-683915426116
contourf(abs.(Δsensitivity_cal), c=:batlow, levels=5)

# ╔═╡ b3a063b9-25d2-4048-a2e4-c2ebafca8f8d
md"""
The 2nd percentile of the relative error is $(round(quantile(vec(Δsensitivity_cal_rel*100), 0.02); digits=2))% and the 98th percentile is $(round(quantile(vec(Δsensitivity_cal_rel*100), 0.98); digits=2))%
"""

# ╔═╡ 0f9aa16e-a9a4-4a8d-b6d3-6079b34bcba0
begin
	p_Δsensitivity_cal_rel = contourf(inc_range, time_vec/60, round.(Δsensitivity_cal_rel*100; digits=3), lw=0, xlabel="Incident angle /°", title="Sensitivity relative error", colorbar_title="\nRelative error /%", c=:batlow, clims=(0, 0.12), levels=0:0.01:0.12, framestyle=:box, size=(1.75*default_dpi, 3*default_dpi))
end

# ╔═╡ d941ee22-93e0-4a61-9cdf-552649cf1de8
if save_figures
	savefig(p_Δsensitivity_cal_rel, joinpath(plots_dir, "rel_error_calibrated.svg"))
end

# ╔═╡ 11bceee7-93fa-4136-a013-f7a4311ae9e5
p_sensitivity_and_error_cal = plot(p_sensitivity_sp, p_Δsensitivity_cal_rel, layout=(1,2))

# ╔═╡ f95725f4-d337-4388-9329-b99bb0375fdd
if save_figures
	savefig(p_sensitivity_and_error_cal, joinpath(plots_dir, "sensitivity_and_error_calibrated.svg"))
end

# ╔═╡ e16eb222-8b6d-4e43-876e-5a8f904c3af1
md"""
#### Geometric method - Without wavenumber scaling
"""

# ╔═╡ 0df2480b-bfb9-4dea-bec1-15e8ba140a6d
_, _, sensitivity_geometric_cal_inc_no_scaling, _ = InterferometricParameterSims.run_temporal_shift_geometric(parameters; conventional_inc=false, los_scaling=false)

# ╔═╡ 0b2aa215-b610-4281-aaea-05208a4f5109
begin
	Δsensitivity_cal_no_sc = abs.(abs.(sensitivity_sp) .- abs.(sensitivity_geometric_cal_inc_no_scaling))
	Δsensitivity_cal_rel_no_sc = Δsensitivity_cal_no_sc ./ abs.(sensitivity_sp)
end

# ╔═╡ a0e960b6-dd29-4360-bfaf-7dc2a9a20c27
md"""
The 2nd percentile of the relative error is $(round(quantile(vec(Δsensitivity_cal_rel_no_sc*100), 0.02); digits=2))% and the 98th percentile is $(round(quantile(vec(Δsensitivity_cal_rel_no_sc*100), 0.99); digits=2))%
"""

# ╔═╡ 3cea388a-328c-47f9-9ed3-08066fc83557
begin
	p_Δsensitivity_cal_rel_no_sc = contourf(inc_range, time_vec/60, round.(Δsensitivity_cal_rel_no_sc*100; digits=3), lw=0, xlabel="Incident angle /°", title="Sensitivity relative error", colorbar_title="\nRelative error /%", c=:batlow, clims=(1.4, 2.4), levels=1.4:0.2:2.4, framestyle=:box, size=(1.75*default_dpi, 3*default_dpi))
end

# ╔═╡ a1df559d-eaf8-43eb-9af8-05d14d1358bc
if save_figures
	savefig(p_Δsensitivity_cal_rel_no_sc, joinpath(plots_dir, "rel_error_calibrated_no_sc.svg"))
end

# ╔═╡ 4f4d4394-8698-41c1-9a06-aa47cd473b6e
md"""
### Geometric method - ME at midpoint
"""

# ╔═╡ f2dd0c15-3e89-46a0-840b-ffb6e1267c9b
_, sensitivity_drama, _ = InterferometricParameterSims.run_temporal_shift_drama_method(parameters)

# ╔═╡ 0bac6e66-2fb6-4532-96fd-6369ba22ac6f
contourf(inc_range, time_vec/60, abs.(sensitivity_geometric), lw=0, xlabel="Incident angle /°", ylabel="Time in orbit /min", title="Sensitivity to height", colorbar_title="\nSensitivity /rad/m", c=:batlow, clims=(0, 0.18), levels=0:.02:0.18, framestyle=:box)

# ╔═╡ eaca4ced-4c26-49b2-81b7-c90cb05fc150
begin
	Δsensitivity_sp_drama = abs.(abs.(sensitivity_sp) .- abs.(sensitivity_drama))
	Δsensitivity_rel_sp_drama = Δsensitivity_sp_drama ./ abs.(sensitivity_sp)
end

# ╔═╡ 19edb3ab-7010-4888-ba7b-947cc694fdff
p_Δsensitivity_rel_sp_drama = contourf(inc_range, time_vec/60, Δsensitivity_rel_sp_drama*100, lw=0, xlabel="Incident angle /°", title="Sensitivity relative error", colorbar_title="\nRelative error /%", c=:batlow, clims=(0, 6), levels=0:6, framestyle=:box)

# ╔═╡ eb15ed6d-2763-4080-9f73-75521d1b6f0f
p_sensitivity_and_error_drama = plot(p_sensitivity_sp, p_Δsensitivity_rel_sp_drama, layout=(1,2))

# ╔═╡ 290c696d-d53e-43e4-bf03-ddc3434d984e
md"""
The maximum relative error in the region where the orbits do not cross is $(round(maximum(Δsensitivity_rel_sp_drama[270:2900, :])*100; digits=2))%.
"""

# ╔═╡ 85d1dcc4-531a-4170-af51-8336e1f99657
contourf(Δsensitivity_rel_sp_drama[270:2900, :]*100, lw=0, xlabel="Incident angle /°", title="Sensitivity relative error", colorbar_title="\nRelative error /%", c=:batlow, clims=(0,7), levels=0:7)

# ╔═╡ a790a1c3-e1d6-4df0-946e-c5d1e7605e19
b_perp = InterferometricParameterSims.run_parallel_perpendicular_baseline_p(parameters)

# ╔═╡ 02d5471c-799b-4656-8b92-097f5c547714
contourf(abs.(b_perp), c=:batlow, levels=0:60:600)

# ╔═╡ ddcf597a-c060-4fbb-bef0-4d61265f068f
contourf(abs.(b_perp_geometric), c=:batlow, levels=0:60:600)

# ╔═╡ 60afe46a-0029-4945-9be6-21579298192b
begin
	Δb_perp = abs.(b_perp - b_perp_geometric)
	Δb_perp_rel = Δb_perp ./ b_perp
end

# ╔═╡ cecd22c7-77e6-4ddb-b84a-1670b8080f41
contourf(Δb_perp, c=:batlow, clims=(0, 27), levels=0:3:27)

# ╔═╡ 1d7294ef-17f2-418f-9aec-3f20ac9823bb
contourf(abs.(Δb_perp_rel)*100, c=:batlow, clims=(0, 9))

# ╔═╡ d45b8817-c6ca-478b-b49b-cec0772bb2e4
begin
	Δb_perp_cal = abs.(b_perp - b_perp_geom_cal)
	Δb_perp_cal_rel = Δb_perp ./ b_perp
end

# ╔═╡ 58b46dc2-dcd0-4d27-b66a-8bee3348e468
contourf(abs.(Δb_perp_cal_rel)*100, c=:batlow, clims=(0, 9))

# ╔═╡ Cell order:
# ╠═d6aaf3d0-c097-4995-8dbd-5ae1d1842dc6
# ╟─f8e8b47d-703e-421b-bc99-6d71ed269b81
# ╟─664d8a9e-d440-4c9c-9780-b6a9dd15580e
# ╠═978e10cc-700b-4d26-9d18-5417e77f5361
# ╟─22c5dbeb-ce81-4924-8296-d2e1c1aa260d
# ╠═cb2e0153-f136-4f44-9442-13634e26419c
# ╟─704658e6-df87-4ba0-babf-2092acec7e88
# ╟─7f0ff276-42ec-496b-9082-bc040311bf86
# ╟─227757bb-bc29-4f4a-9fe1-8df697986bb0
# ╠═4d94a6ce-8201-438f-a3f9-053733bd8eef
# ╟─c66e73e0-86b3-4bb7-be01-830927a85802
# ╟─5cb26618-40e8-4b4f-9b62-2cfc19f4b1bb
# ╠═512d0b42-a67a-4258-abf5-790143cb4efd
# ╟─768ccbb2-cccc-44ee-a282-24d9a7953504
# ╠═7f5a6772-c6f5-4d21-b23f-ff4226f762d9
# ╟─7aa92890-0a75-4a5b-9806-21cb53e75d5a
# ╠═287976e0-c9bb-4391-934b-6fd2c7fb8dc2
# ╠═ec22824d-1b88-48c4-857e-4dc8341e4b63
# ╟─a3597ce5-c699-4f91-93d4-6fcb991da4aa
# ╠═22b2d51e-b7c0-41b5-a61a-12387c46a3ce
# ╠═57f1277a-928d-494d-94c0-a54170014bc2
# ╠═2060f79e-2948-4392-8d21-2ccac319053e
# ╠═7c32aa98-2a46-4d1e-ae83-015204e05e4a
# ╠═a6853612-81e6-40ee-9801-d00f03841725
# ╟─964ed01f-f535-4305-b399-de1bfb943707
# ╠═68ca9815-d9b9-40c0-862a-dc1c4b4f6a85
# ╠═9f045fc0-684c-44e0-b9ed-e3802a21b325
# ╠═ba4e88da-a53c-471f-b790-13005f288b4f
# ╟─2cc467a1-0d8c-4396-acdd-98bbeb7b49fe
# ╠═68ce6113-a834-42fd-b8fc-9df04af5ea39
# ╟─6771d8ec-1196-496f-9705-8c4fecf0ccac
# ╠═87ae022d-fbd5-481e-a3ca-f0bbb73a7384
# ╠═c6e04f62-8ea5-4e46-b5f2-6b8465507437
# ╠═edc290e5-e115-4e68-858d-e3dcdbf0c40b
# ╟─5ab28385-8563-45d1-b5eb-77db3b446e79
# ╠═2dadea49-f53b-4e23-a070-6b9efea79ceb
# ╟─706343a8-793b-433e-8e7b-335b53b4967f
# ╠═33ed325b-c4d6-4777-98ba-1c153b9c1a0f
# ╟─abee8f7b-da5b-49e8-9218-599f5f36d2b2
# ╠═583edd33-eb24-48e4-ae5f-b48cf9a6184b
# ╠═b1339794-cf6a-4216-a5b5-30f49466e39b
# ╠═16ef7591-e60c-4410-817d-601caf63d918
# ╠═ee737cfe-22a7-4cce-9f20-1d3ded3e9f00
# ╠═e107fe9e-bf7c-49e1-8963-02461008ab95
# ╟─b1305eb4-047a-4b43-99b5-7384a6427658
# ╟─28e0a792-62de-4d3b-92b1-51be58373b67
# ╠═3f545581-bfed-4852-a3e9-105b97c73662
# ╠═de6c8a42-62d6-4b94-bccc-8c4758c2fee3
# ╠═333c4a63-d572-4520-a156-e513e2ce1ebc
# ╟─12e5834c-ff04-4d4a-8d5d-92dd9f38595b
# ╠═be9d7df8-9a3d-44e6-a09e-50a564932f32
# ╟─ae7c596c-b9a2-4b17-bd6e-b25cdd92d09b
# ╠═d4adf3b2-3ad1-4c92-8f62-90190cfd3cb1
# ╠═ced822e9-25c1-494d-8cce-7c75739118d9
# ╟─164d4be4-fa4b-41e1-b313-634b2f396a6f
# ╟─3cad3068-49b9-4c9a-ab24-83742c74496a
# ╟─cb396a3c-3ddf-4a82-92e8-d8641bdaed55
# ╠═a5fa6096-b9ca-43a7-aaca-fd25b7966b35
# ╟─d80314b7-5340-4ee1-bd7c-145813b4fd3d
# ╠═18a69d19-05bf-48b7-bb23-6c1c6280410d
# ╟─bb942269-c415-46d5-a179-82f4576b5c23
# ╠═aa1e39e5-ff48-4718-9820-016ba3c1424b
# ╟─5ac13f63-065d-4e4d-a4e2-ffc10e2f2ad5
# ╟─bfb5fa01-2a9b-4941-a924-81e793af8e68
# ╟─ec952303-270a-44e2-9b3c-93eb1b1d136c
# ╟─35034430-d6a1-4e61-9cc9-9bb2d2769134
# ╟─1d9e2aed-3ba6-4a97-97b6-05ecf0e9af04
# ╠═b72a1a57-377a-4ca6-bbe6-f7a474d60fb0
# ╠═a5e75057-13b5-40af-a09e-4c4fc7050026
# ╠═f6c97db0-6622-4dc9-8522-b210776a0284
# ╟─2cd870d6-5767-498d-ad72-f5573d4b7fae
# ╠═0f584729-54e4-4321-8919-a69cdf1e8018
# ╠═5c42babb-71e9-46f8-81ce-7fd538e9b9ea
# ╠═c4b2af99-8525-422f-82ec-aba9f354d280
# ╠═ab477815-27b2-47d3-a948-46a62e10c81e
# ╠═2627112c-e834-4288-b28b-b74383ae9bf6
# ╠═4761b4b6-1e82-4acf-8ebf-81036f5bd794
# ╠═fc9db39e-a4cc-4292-a407-29193f7add64
# ╠═7323b8a4-6680-4c20-b2f7-844f8270e62d
# ╠═55cd80c8-a3f6-4ab3-9b5c-9b24bf97a04e
# ╟─f1338d3b-9f0c-4d83-9ec1-fb630a3cf2fa
# ╠═fb1b8c7e-076e-49f9-9c49-7ee0ee3b851a
# ╠═1a24eaa7-a26f-4076-b443-5511176d30bc
# ╠═d7da8b59-295b-4abb-9bb9-fbe1d14fd798
# ╠═2d5f3334-a045-4985-891b-3e64dff76520
# ╠═9eca0cbe-1f5a-44c8-a444-a1bd4fa23baa
# ╠═ed495580-6719-45f6-9806-3d4f8ecec597
# ╠═6158312a-9f1f-4cee-8caf-97701ef70829
# ╟─ad685b43-4971-4a9f-b4cf-4010a0787794
# ╠═a76ab46b-737b-4c61-af07-3bd038b9c8b3
# ╟─321315f9-ca3b-4829-b441-6734284c088a
# ╠═294bf5d3-3aba-46b2-863b-95cf5c0e2e1d
# ╠═8c86a59c-f7b3-45ac-8829-5847595941f4
# ╟─abe7d587-389c-44bd-82f3-daa768a1a782
# ╠═14fe0e89-ec6e-4307-a1e0-28938ce6c4ef
# ╠═093ace7d-e5ef-47ed-aaa1-c873c840f809
# ╠═bcd1c3e2-51df-4056-8b8b-d96bde7e6fc7
# ╟─6d36b020-d049-4aa4-9024-bfa4000675ca
# ╟─34245985-4d17-432c-b780-b044d2104e59
# ╠═04958ba0-a1a3-463b-b402-b2cdda88aa46
# ╟─5afba60e-3649-495d-9381-21cf1ce3e214
# ╠═0cdd26b8-9531-4f27-b419-dc4a93bf701c
# ╠═3d17259b-7106-4f93-a63c-0bbd524a0d60
# ╠═0449acd8-20e9-4963-80da-2edc8d11c845
# ╟─7295121b-32dc-406a-92ae-708598152f31
# ╠═0ee91179-93ec-4e83-bcf7-d8e6565b618e
# ╠═b490bb28-209d-4865-9294-6e5b53140f56
# ╠═d53dd8f5-0b58-42dc-a3d8-683915426116
# ╟─b3a063b9-25d2-4048-a2e4-c2ebafca8f8d
# ╠═0f9aa16e-a9a4-4a8d-b6d3-6079b34bcba0
# ╠═d941ee22-93e0-4a61-9cdf-552649cf1de8
# ╠═11bceee7-93fa-4136-a013-f7a4311ae9e5
# ╠═f95725f4-d337-4388-9329-b99bb0375fdd
# ╟─e16eb222-8b6d-4e43-876e-5a8f904c3af1
# ╠═0df2480b-bfb9-4dea-bec1-15e8ba140a6d
# ╠═0b2aa215-b610-4281-aaea-05208a4f5109
# ╟─a0e960b6-dd29-4360-bfaf-7dc2a9a20c27
# ╟─3cea388a-328c-47f9-9ed3-08066fc83557
# ╠═a1df559d-eaf8-43eb-9af8-05d14d1358bc
# ╟─4f4d4394-8698-41c1-9a06-aa47cd473b6e
# ╠═f2dd0c15-3e89-46a0-840b-ffb6e1267c9b
# ╠═0bac6e66-2fb6-4532-96fd-6369ba22ac6f
# ╠═eaca4ced-4c26-49b2-81b7-c90cb05fc150
# ╠═19edb3ab-7010-4888-ba7b-947cc694fdff
# ╠═eb15ed6d-2763-4080-9f73-75521d1b6f0f
# ╟─290c696d-d53e-43e4-bf03-ddc3434d984e
# ╠═85d1dcc4-531a-4170-af51-8336e1f99657
# ╠═a790a1c3-e1d6-4df0-946e-c5d1e7605e19
# ╠═02d5471c-799b-4656-8b92-097f5c547714
# ╠═ddcf597a-c060-4fbb-bef0-4d61265f068f
# ╠═60afe46a-0029-4945-9be6-21579298192b
# ╠═cecd22c7-77e6-4ddb-b84a-1670b8080f41
# ╠═1d7294ef-17f2-418f-9aec-3f20ac9823bb
# ╠═d45b8817-c6ca-478b-b49b-cec0772bb2e4
# ╠═58b46dc2-dcd0-4d27-b66a-8bee3348e468
