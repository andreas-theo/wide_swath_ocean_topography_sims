using InterferometricParameterSims
using Test

function vec_of_field(vec_of_structure, field_name::String)
    return map(s -> getfield(s, Symbol(field_name)), vec_of_structure)
end

@testset "InterferometricParameterSims.jl" begin
end
