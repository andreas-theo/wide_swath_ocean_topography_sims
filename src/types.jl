################################################################################
#                                    Sim
################################################################################

export Sim

"""
    Sim

This structure defines the parameters that describe a simulation to find the region of support of a passive bistatic InSAR.

# Fields

* `main_orb_tle`: Two-line element set of the illuminator.
* `ref_companion`: The orbit of the reference companion. Describes the orbit relative to that of the illuminator. This companion is used as a reference to the second companion.
* `chaser_companion`: The orbit of the chaser companion. Its orbit is described relative to the reference companion.
* `inc_range`: The range of incidence angles for which to compute the line of sight of the illuminator [rad].
* `fr`: The range frequency of the illuminator. The vector describes the range of offset frequencies with respect to the centre frequency of the illuminator. A linear chirp is assumed. [Hz].
* `end_t`: The total time for which the orbit is analysed, as a fraction of the orbital period.
* `propagation_window_length`: The length of time of each simulation window [s].
* `dt`: The spacing between samples in each window [s].

"""
struct Sim
    main_orb_tle::String
    ref_companion::RelativeOrbit
    chaser_companion::RelativeOrbitalElements
    inc_range::AbstractVector
    fr::AbstractVector
    end_t_fraction::Float64
    propagation_window_length::Real
    dt::Float64
end
