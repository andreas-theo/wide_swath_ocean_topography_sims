module InterferometricParameterSims

using Printf
using SARToolbox
using SatelliteToolbox
import Folds
import ProgressMeter
import NetCDF
using StaticArrays
import Dates
using LinearAlgebra
using Tullio
using Transducers: withprogress

include("from_drama.jl")
include("types.jl")
include("spectral_shift.jl")

end
