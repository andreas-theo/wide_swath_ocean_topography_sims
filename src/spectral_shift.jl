function monostatic_setup(
    inc_range,
    fr,
    end_t = 0.5,
    propagation_window_length = 1,
    dt = 1e-4,
)
    orbp = SARToolbox.create_reference_orbit("assets/sentinel-1B")
    orbp_h = deepcopy(orbp)
    Sim(
        "assets/sentinel-1B",
        RelativeOrbit(0, 0, 0, 0, 0, 0),
        RelativeOrbitalElements(0, 0, 0, 50 / Propagators.mean_elements(orbp_h).a, 0, 0),
        inc_range,
        fr,
        end_t,
        propagation_window_length,
        dt,
    )
end

function monostatic_setup_only_normal_separation(
    inc_range,
    fr,
    end_t = 0.5,
    propagation_window_length = 1,
    dt = 1e-4,
    )
    orbp = SARToolbox.create_reference_orbit("assets/sentinel-1B")
    orbp_h = deepcopy(orbp)
    Sim(
        "assets/sentinel-1B",
        RelativeOrbit(0, 0, 0, 0, 0, 0),
        RelativeOrbitalElements(0, 0, 0, 0, 0, 650 / Propagators.mean_elements(orbp_h).a * sin(Propagators.mean_elements(orbp_h).i)),
        inc_range,
        fr,
        end_t,
        propagation_window_length,
        dt,
    )
end

function bistatic_setup(
    inc_range,
    fr,
    end_t = 0.5,
    propagation_window_length = 1,
    dt = 1e-4,
)
    orbp = SARToolbox.create_reference_orbit("assets/sentinel-1B")
    orbp_h = SARToolbox.from_rel_orbit(
        Val(:J2),
        Propagators.mean_elements(orbp),
        0,
        0,
        -SatelliteToolbox.mean_to_true_anomaly(Propagators.mean_elements(orbp).e, 350e3 / Propagators.mean_elements(orbp).a),
    )
    Sim(
        "assets/sentinel-1B",
        RelativeOrbit(
            0,
            0,
            0,
            0,
            0,
            -SatelliteToolbox.mean_to_true_anomaly(Propagators.mean_elements(orbp).e, 350e3 / Propagators.mean_elements(orbp).a),
        ),
        RelativeOrbitalElements(0, 0, 0, 150 / Propagators.mean_elements(orbp_h).a, 0, -650 / Propagators.mean_elements(orbp_h).a * sin(Propagators.mean_elements(orbp_h).i)),
        # RelativeOrbitalElements(0, 0, 0, 50 / Propagators.mean_elements(orbp_h).a, 0, 0),
        inc_range,
        fr,
        end_t,
        propagation_window_length,
        dt,
    )
end

function bistatic_setup_industry(
    inc_range,
    fr,
    end_t = 0.5,
    propagation_window_length = 1,
    dt = 1e-4,
)
    orbp = SARToolbox.create_reference_orbit("assets/sentinel-1B")
    orbp_h = SARToolbox.from_rel_orbit(
        Val(:J2),
        Propagators.mean_elements(orbp),
        0,
        0,
        -SatelliteToolbox.mean_to_true_anomaly(Propagators.mean_elements(orbp).e, 1000e3 / Propagators.mean_elements(orbp).a),
    )
    Sim(
        "assets/sentinel-1B",
        RelativeOrbit(
            0,
            0,
            0,
            0,
            0,
            -SatelliteToolbox.mean_to_true_anomaly(Propagators.mean_elements(orbp).e, 1000e3 / Propagators.mean_elements(orbp).a),
        ),
        RelativeOrbitalElements(0, 0, 0, 750 / Propagators.mean_elements(orbp_h).a, 0, 2100 / Propagators.mean_elements(orbp_h).a * sin(Propagators.mean_elements(orbp_h).i)),
        # RelativeOrbitalElements(0, 0, 0, 50 / Propagators.mean_elements(orbp_h).a, 0, 0),
        inc_range,
        fr,
        end_t,
        propagation_window_length,
        dt,
    )
end

function bistatic_setup_geo(
    inc_range,
    fr,
    end_t = 0.5,
    propagation_window_length = 1,
    dt = 1e-4,
)
    orbp = SARToolbox.create_reference_orbit("assets/sentinel-1B")
    orbp_h = SARToolbox.from_rel_orbit(
        Val(:J2),
        Propagators.mean_elements(orbp),
        0,
        0,
        -SatelliteToolbox.mean_to_true_anomaly(Propagators.mean_elements(orbp).e, 350e3 / Propagators.mean_elements(orbp).a),
    )
    Sim(
        "assets/sentinel-1B",
        RelativeOrbit(
            0,
            0,
            0,
            0,
            0,
            -SatelliteToolbox.mean_to_true_anomaly(Propagators.mean_elements(orbp).e, 350e3 / Propagators.mean_elements(orbp).a),
        ),
        RelativeOrbitalElements(0, 0, 0, 110 / Propagators.mean_elements(orbp_h).a, 0, 650 / Propagators.mean_elements(orbp_h).a * sin(Propagators.mean_elements(orbp_h).i)),
        # RelativeOrbitalElements(0, 0, 0, 50 / Propagators.mean_elements(orbp_h).a, 0, 0),
        inc_range,
        fr,
        end_t,
        propagation_window_length,
        dt,
    )
end

function bistatic_setup_only_λ(
    inc_range,
    fr,
    end_t = 0.5,
    propagation_window_length = 1,
    dt = 1e-4,
)
    orbp = SARToolbox.create_reference_orbit("assets/sentinel-1B")
    orbp_h = SARToolbox.from_rel_orbit(
        Val(:J2),
        Propagators.mean_elements(orbp),
        0,
        0,
        -SatelliteToolbox.mean_to_true_anomaly(Propagators.mean_elements(orbp).e, 300e3 / Propagators.mean_elements(orbp).a),
    )

    Sim(
        "assets/sentinel-1B",
        RelativeOrbit(
            0,
            0,
            0,
            0,
            0,
            -SatelliteToolbox.mean_to_true_anomaly(Propagators.mean_elements(orbp).e, 300e3 / Propagators.mean_elements(orbp).a),
        ),
        RelativeOrbitalElements(0, 10 / Propagators.mean_elements(orbp_h).a, 0, 0, 0, 0),
        inc_range,
        fr,
        end_t,
        propagation_window_length,
        dt,
    )
end

function calculate_companion_delta(parameters::Sim)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h1 = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp) + parameters.ref_companion,
    )
    harmony_formation = parameters.chaser_companion
    asc_t = SARToolbox.ascend_descend(orbp, 0.001)
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    time_vector = asc_t:1:asc_t+T
    o_h1, r_h1, _ = SARToolbox.propagate_with_mean_elements!(orbp_h1, time_vector)
    Δr = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr[i] = SARToolbox.relative_orb_to_Δr(harmony_formation, o_h1[i].a, u)
    end
    Δr
end

function satellites_and_ground_point(parameters::Sim)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h1 = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp) + parameters.ref_companion,
    )
    harmony_formation = parameters.chaser_companion
    asc_t = SARToolbox.ascend_descend(orbp, 0.001)
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    time_vector = asc_t:parameters.dt:asc_t+parameters.propagation_window_length
    o_s1, r_s1, v_s1 = SARToolbox.propagate_with_mean_elements!(orbp, time_vector)
    o_h1, r_h1, v_h1 = SARToolbox.propagate_with_mean_elements!(orbp_h1, time_vector)
    r_h2 = SARToolbox.propagate_companion(o_h1, r_h1, v_h1, harmony_formation)
    r_s1_e, v_s1_e_mid, r_h1_e, r_h2_e = SARToolbox.satellites_to_ECEF(o_s1, r_s1, v_s1, r_h1, r_h2)
    mid_index = floor(Int, length(r_s1_e) / 2)
    Q_midpoint = @views SARToolbox.ECI_to_LVLH(r_s1_e[mid_index], v_s1_e_mid)
    Q_midpoint = transpose(Q_midpoint)
    r_p = SARToolbox.ground_point_from_position(r_s1_e, Q_midpoint, parameters.inc_range)
    (r_s1_e, r_h1_e, r_h2_e, r_p)
end


function run_parallel_temporal_shift_rel(parameters::Sim, search_window_length::Int)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp) + parameters.ref_companion,
    )
    harmony_formation = parameters.chaser_companion
    inc_range = parameters.inc_range
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    asc_t = SARToolbox.ascend_descend(orbp_h, 0.001)
    time_vectors = (
        t:parameters.dt:t+parameters.propagation_window_length-parameters.dt for t =
            asc_t:parameters.propagation_window_length:(asc_t+T*parameters.end_t_fraction)
    )
    res = ProgressMeter.progress_map(
        t -> SARToolbox.temporal_diff(
            (orbp, orbp_h),
            harmony_formation,
            inc_range,
            parameters.fr |> collect,
            t,
            search_window_length,
        ),
        time_vectors;
        mapfun = Folds.map,
        progress = ProgressMeter.Progress(length(time_vectors)),
        channel_bufflen = min(1000, length(time_vectors)),
    )
    return res
end

function run_parallel_temporal_shift_rel_analytical(parameters::Sim)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp) + parameters.ref_companion,
    )
    harmony_formation = parameters.chaser_companion
    inc_range = parameters.inc_range
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    asc_t = SARToolbox.ascend_descend(orbp_h, 0.001)
    time_vector = asc_t:parameters.propagation_window_length:(asc_t+T*parameters.end_t_fraction)
    time_inc_combinations = Iterators.product(time_vector, inc_range)
    res = ProgressMeter.progress_map(
        ((t, inc_angle),) -> SARToolbox.temporal_diff_analytical(
            (orbp, orbp_h),
            harmony_formation,
            inc_angle,
            t,
        ),
        time_inc_combinations;
        mapfun=Folds.map,
        progress=ProgressMeter.Progress(length(time_inc_combinations)),
        channel_bufflen=min(1000, length(time_inc_combinations))
    )
    return res
end

function run_parallel_temporal_shift_rel_analytical_p(parameters::Sim, monostatic=false)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp) + parameters.ref_companion,
    )
    harmony_formation = parameters.chaser_companion
    inc_range = parameters.inc_range
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    asc_t = SARToolbox.ascend_descend(orbp_h, 0.001)
    time_vector = asc_t:parameters.propagation_window_length:(asc_t+T*parameters.end_t_fraction)
    time_inc_combinations = Iterators.product(time_vector, inc_range)
    if monostatic
        res = Folds.map(withprogress(time_inc_combinations)) do (t, θ)
            SARToolbox.temporal_diff_analytical_mono((orbp, orbp_h), harmony_formation, θ, t)
        end
    else
        res = Folds.map(withprogress(time_inc_combinations)) do (t, θ)
            SARToolbox.temporal_diff_analytical((orbp, orbp_h), harmony_formation, θ, t)
        end
    end
    return res
end

function run_parallel_sensitivity_analytical_p(parameters::Sim, monostatic=false)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp) + parameters.ref_companion,
    )
    harmony_formation = parameters.chaser_companion
    inc_range = parameters.inc_range
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    asc_t = SARToolbox.ascend_descend(orbp_h, 0.001)
    time_vector = asc_t:parameters.propagation_window_length:(asc_t+T*parameters.end_t_fraction)
    time_inc_combinations = Iterators.product(time_vector, inc_range)
    if monostatic
        res = Folds.map(withprogress(time_inc_combinations)) do (t, θ)
            SARToolbox.sensitivity_analytical_mono((orbp, orbp_h), harmony_formation, θ, t)
        end
    else
        res = Folds.map(withprogress(time_inc_combinations)) do (t, θ)
            SARToolbox.sensitivity_analytical((orbp, orbp_h), harmony_formation, θ, t)
        end
    end
    return res
end

function run_parallel_perpendicular_baseline_p(parameters::Sim)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp) + parameters.ref_companion,
    )
    harmony_formation = parameters.chaser_companion
    inc_range = parameters.inc_range
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    asc_t = SARToolbox.ascend_descend(orbp_h, 0.001)
    time_vector = asc_t:parameters.propagation_window_length:(asc_t+T*parameters.end_t_fraction)
    time_inc_combinations = Iterators.product(time_vector, inc_range)
    res = Folds.map(withprogress(time_inc_combinations)) do (t, θ)
        SARToolbox.perpendicular_baseline((orbp, orbp_h), harmony_formation, θ, t)
    end
    return res
end

function run_parallel_temporal_shift_rel_flat(parameters::Sim, search_window_length::Int, separate_iluminator=false)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp) + parameters.ref_companion,
    )
    inc_range = parameters.inc_range
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    asc_t = SARToolbox.ascend_descend(orbp, 0.001)
    o_h1, _, v_h1 = SARToolbox.propagate_with_mean_elements!(orbp_h, asc_t:parameters.dt:asc_t+T*parameters.end_t_fraction)
    r_h1 = zero(v_h1)
    v_h1 = LinearAlgebra.norm.(v_h1)
    r_h1[1] = @SVector [693E3, 0.0, 0.0]
    # iterate over all indices except the first one
    for i in Iterators.drop(eachindex(r_h1), 1)
        r_h1[i] = @SVector [693E3, r_h1[i-1][2] + parameters.dt * v_h1[i-1], 0]
    end
    if separate_iluminator
        aΔu = Propagators.mean_elements(orbp).a * (mod2pi((SatelliteToolbox.true_to_mean_anomaly(Propagators.mean_elements(orbp).e, parameters.ref_companion.Δf) + π/2)) - π/2)
        r_s1 = r_h1 .- ([0, aΔu, 0],)
    end
    r_h2 = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr = SARToolbox.relative_orb_to_Δr(parameters.chaser_companion, o_h1[i].a, u)
        r_h2[i] = r_h1[i] + Δr
    end
    samples_in_window = floor(Int, parameters.propagation_window_length / parameters.dt)
    indices = [i:i+samples_in_window for i in firstindex(r_h1):samples_in_window:lastindex(r_h1)-samples_in_window]
    if !separate_iluminator
        res = ProgressMeter.progress_map(
            i -> SARToolbox.temporal_diff_flat(
                r_h1[i],
                r_h2[i],
                inc_range,
                parameters.fr |> collect,
                parameters.dt,
                search_window_length,
            ),
            indices;
            mapfun = map,
            progress = ProgressMeter.Progress(length(indices)),
            channel_bufflen = min(1000, length(indices)),
        )
    else
        res = ProgressMeter.progress_map(
            i -> SARToolbox.temporal_diff_flat_separate_illuminator(
                r_s1[i],
                r_h1[i],
                r_h2[i],
                inc_range,
                parameters.fr |> collect,
                parameters.dt,
                search_window_length,
            ),
            indices;
            mapfun = map,
            progress = ProgressMeter.Progress(length(indices)),
            channel_bufflen = min(1000, length(indices)),
        )
    end
    return res
end

function run_parallel_temporal_shift_rel_flat_analytical(parameters::Sim)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp) + parameters.ref_companion,
    )
    inc_range = parameters.inc_range
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    asc_t = SARToolbox.ascend_descend(orbp, 0.001)
    o_h1, _, v_h1 = SARToolbox.propagate_with_mean_elements!(orbp_h, asc_t:parameters.propagation_window_length:asc_t+T*parameters.end_t_fraction)
    r_h1 = zero(v_h1)
    v_h1 = LinearAlgebra.norm.(v_h1)
    r_h1[1] = @SVector [693E3, 0.0, 0.0]
    # iterate over all indices except the first one
    for i in Iterators.drop(eachindex(r_h1), 1)
        r_h1[i] = @SVector [693E3, r_h1[i-1][2] + parameters.dt * v_h1[i-1], 0]
    end
    v_h1 = [@SVector [0, v, 0] for v in v_h1]
    r_h2 = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr = SARToolbox.relative_orb_to_Δr(parameters.chaser_companion, o_h1[i].a, u)
        r_h2[i] = r_h1[i] + Δr
    end
    time_indices = eachindex(o_h1)
    time_inc_combinations = Iterators.product(time_indices, inc_range)
    res = map(
        ((i_t, inc_angle),) -> SARToolbox.sensitivity_flat_analytical(
            r_h1[i_t],
            r_h2[i_t],
            v_h1[i_t],
            inc_angle,
        ),
        time_inc_combinations
    )
    return res
end

function run_store_to_nc(description::String, parameters::Sim, search_window_length::Int)
    println("Running simulation 🚀🛰")
    res = run_parallel_temporal_shift_rel(parameters, search_window_length)
    res_low_f = map(x -> x[1, :], res)
    zs = [res_low_f[i][j] for i = 1:length(res_low_f), j = 1:length(res_low_f[1])]

    println("Calculating ground track of S-1.")
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    asc_t = SARToolbox.ascend_descend(orbp, 0.001)
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    search_spacing_factor = 3
    time_vectors = [
        t + parameters.propagation_window_length / 2 for t =
            asc_t:parameters.propagation_window_length*search_spacing_factor:(asc_t+T*parameters.end_t_fraction)
    ]
    o_s1, r_s1, _ = SARToolbox.propagate_with_mean_elements!(orbp, time_vectors)
    lat_lon = SARToolbox.ground_track.(map(x -> x.t, o_s1), r_s1)

    println("Preparing NetCDF file.")
    incatts = Dict("longname" => "Angle of incidence of S-1", "units" => "rad")
    latatts = Dict("longname" => "Latitude", "units" => "°")
    varatts = Dict(
        "longname" => "Temporal delay for minimum spectral support distance",
        "units" => "s",
    )
    fn = joinpath(
        "assets/",
        "temporal_delay_$(description)_window_$(search_window_length)_samples_fr_$(parameters.fr[1])_$(parameters.fr[end])_inc_$(@sprintf("%.0f", rad2deg(parameters.inc_range[1])))_$(@sprintf("%.0f", rad2deg(parameters.inc_range[end])))_search_spacing_$(search_spacing_factor).nc",
    )
    NetCDF.nccreate(
        fn,
        "t_at",
        "lat",
        map(x -> rad2deg(x[1]), lat_lon),
        latatts,
        "inc",
        parameters.inc_range,
        incatts,
        atts = varatts,
    )
    NetCDF.ncwrite(zs, fn, "t_at")
    return fn
end

function read_vec3(fn::String, var_name::String; subscripts=("_x", "_y", "_z"), count=[-1,])
    x, y, z = (
        NetCDF.ncread(fn, var_name * subscripts[1], count=count),
        NetCDF.ncread(fn, var_name * subscripts[2], count=count),
        NetCDF.ncread(fn, var_name * subscripts[3], count=count),
    )
    Rpre = CartesianIndices(x)
    [@SVector [x[Ipre], y[Ipre], z[Ipre]] for Ipre in Rpre]
end

function run_parallel_temporal_shift_drama(parameters::Sim, search_window_length::Int)
    r_s1_e, v_s1_e, o_h1, r_h1_e, v_h1_e, lats, ts, _, inc_range =
        get_orbits("Hrmny_XTI_model_support_monostatic.cfg")
    harmony_formation = parameters.chaser_companion
    inc_range = parameters.inc_range
    dt_source = ts[2] - ts[1]
    dt_itp = parameters.dt
    itp_ts = (
        t:dt_itp:t+parameters.propagation_window_length for
        t in ts[1:floor(Int, parameters.propagation_window_length / dt_source):end]
    )
    indices = (
        findall(x -> (x >= t_i) && (x <= (t_i + parameters.propagation_window_length)), ts) for
        t_i in ts[1:floor(Int, parameters.propagation_window_length / dt_source):end]
    )
    res = ProgressMeter.progress_map(
        (i, itp_t) -> temporal_diff_itp(
            (r_s1_e[i], v_s1_e[i]),
            (o_h1[i], r_h1_e[i], v_h1_e[i]),
            harmony_formation,
            inc_range,
            parameters.fr |> collect,
            itp_t,
            search_window_length,
        ),
        indices,
        itp_ts;
        mapfun = Folds.map,
        progress = ProgressMeter.Progress(length(indices)),
        channel_bufflen = min(1000, length(indices)),
    )
    return res
end

function run_parallel_temporal_shift_rp_drama(
    parameters::Sim,
    search_window_length::Int,
    fn::String,
)
    r_s1_e, v_s1_e, o_h1, r_h1_e, v_h1_e, lats, ts, r_p_e, inc_range = get_orbits(fn)
    harmony_formation = parameters.chaser_companion
    dt_source = round(ts[2] - ts[1], digits = 2)
    dt_itp = parameters.dt
    itp_ts = [
        t:dt_itp:t+parameters.propagation_window_length for
        t in ts[1:round(Int, parameters.propagation_window_length / dt_source):end-1]
    ]
    indices = [
        findall(x -> (x >= t_i) && (x <= (t_i + parameters.propagation_window_length)), ts) for
        t_i in ts[1:round(Int, parameters.propagation_window_length / dt_source):end-1]
    ]
    inc_index = 1:round(Int, deg2rad(1) / (inc_range[2] - inc_range[1])):length(inc_range)
    inc_range = inc_range[inc_index]
    inc_range = range(inc_range[1], inc_range[end], length = length(inc_range))
    r_p_e = r_p_e[:, inc_index]
    res = ProgressMeter.progress_map(
        (i, itp_t) -> temporal_diff_itp(
            (r_s1_e[i], v_s1_e[i]),
            (o_h1[i], r_h1_e[i], v_h1_e[i]),
            harmony_formation,
            r_p_e[i, :],
            inc_range,
            parameters.fr |> collect,
            itp_t,
            search_window_length,
        ),
        indices,
        itp_ts;
        mapfun = Folds.map,
        progress = ProgressMeter.Progress(length(indices)),
        channel_bufflen = min(1000, length(indices)),
    )
    return (res, inc_range, lats)
end

function prun_temporal_shift_from_nc(parameters::Sim, search_window_length::Int, fn::String)
    r_s1_e, v_s1_e, o_h1, r_h1_e, v_h1_e, lats, ts, r_p_e, inc_range =
        get_orbits_from_nc(fn, parameters.end_t_fraction)
    harmony_formation = parameters.chaser_companion
    dt_source = round(ts[2] - ts[1], digits = 2)
    dt_itp = parameters.dt
    itp_ts = [
        t:dt_itp:t+parameters.propagation_window_length for
        t in ts[1:round(Int, parameters.propagation_window_length / dt_source):floor(Int, lastindex(ts))-1]
    ]
    indices = [
        findall(x -> (x >= t_i) && (x <= (t_i + parameters.propagation_window_length)), ts) for
        t_i in ts[1:round(Int, parameters.propagation_window_length / dt_source):floor(Int, lastindex(ts))-1]
    ]
    # inc_index = 1:round(Int, deg2rad(1) / (inc_range[2] - inc_range[1])):length(inc_range)
    # #####################################
    # The following three lines are valid.
    inc_index = range(1, lastindex(inc_range), step=1)
    inc_range = inc_range[inc_index]
    inc_range = range(inc_range[1], inc_range[end], length = length(inc_range))
    # inc_range = parameters.inc_range
    # inc_range = range(inc_range[1], inc_range[end], length = length(inc_range))
    r_p_e = r_p_e[:, inc_index]
    res = ProgressMeter.progress_map(
        (i, itp_t) -> temporal_diff_itp(
            (r_s1_e[i], v_s1_e[i]),
            (o_h1[i], r_h1_e[i], v_h1_e[i]),
            harmony_formation,
            r_p_e[i, :],
            inc_range,
            parameters.fr |> collect,
            itp_t,
            search_window_length,
        ),
        indices,
        itp_ts;
        mapfun = Folds.map,
        progress = ProgressMeter.Progress(length(indices)),
        channel_bufflen = min(1000, length(indices)),
    )
    return (res, inc_range, lats)
end

function temporal_diff_no_itp(
    (r_s1_e, v_s1_e),     # main satellite
    (o_h1, r_h1_e, v_h1_e),     # reference companion satellite
    formation,
    r_p_e,
    inc_range,
    fr,
    dt,
    window_length::Int,
)
    r_h2_e = similar(r_h1_e)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr = SARToolbox.relative_orb_to_Δr(formation, o_h1[i].a, u)
        r_h2_e[i] = SARToolbox.chief_to_deputy_r(r_h1_e[i], v_h1_e[i], Δr)
    end
    sh1, sh2 = (
        SARToolbox.compute_support(r_s1_e, r_h1_e, r_p_e, fr),
        SARToolbox.compute_support(r_s1_e, r_h2_e, r_p_e, fr),
    )
    indices = SARToolbox.spectral_shift_loop(sh1, sh2, 1, window_length)
    [(window_length - i[1]) * dt for i in indices]
end

function find_intersection_with_surface(parameters::Sim, fn::String)
    r_s1_e, v_s1_e, _, _, _, _, ts, _, _ =
        get_orbits_from_nc(fn, parameters.end_t_fraction)
    dt_in_samples = floor(Int, 1 / round(ts[2] - ts[1], digits=2))
    look_angle = deg2rad.(range(16, 42, length=456))
    b̂ = SARToolbox.wave_vector.(look_angle, 0)
    indices = firstindex(ts)+5:dt_in_samples:lastindex(ts)
    r_p = zeros(SVector{3,Float64}, (length(indices), length(look_angle)))
    for (r_p_index, r_s1_index) in enumerate(indices)
        r_s1_mid = @views r_s1_e[r_s1_index]
        Q_midpoint = @views SARToolbox.ECI_to_LVLH(r_s1_mid, v_s1_e[r_s1_index])
        Q_midpoint = transpose(Q_midpoint)
        b̂_ECEF = (Q_midpoint,) .* b̂
        r_p[r_p_index, :] = SARToolbox.los_to_surface.((r_s1_mid,), b̂_ECEF)
    end
    r_p
end

function find_intersection_with_surface_eci(inc_range, r, v)
    # orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle);
    # asc_t = SARToolbox.ascend_descend(orbp, 0.001);
    # o_s1, r_s1, v_s1 = SARToolbox.propagate_with_mean_elements!(orbp, asc_t:1:asc_t+2);
    look_angle = inc_range
    b̂ = SARToolbox.wave_vector.(look_angle, 0)
    Q = SARToolbox.ECI_to_LVLH(r, v)
    Q = transpose(Q)
    b̂_ECI = (Q,) .* b̂
    r_p = SARToolbox.los_to_surface.((r,), b̂_ECI)
    r_p
end

function find_intersection_with_surface_first_eci_then_ecef(parameters)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    asc_t = SARToolbox.ascend_descend(orbp, 0.001)
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    o_s1, r_s1, v_s1 = SARToolbox.propagate_with_mean_elements!(orbp, asc_t:1:asc_t+T)
    r_p = zeros(SVector{3,Float64}, (size(r_s1, 1), size(parameters.inc_range, 1)))
    for i in axes(r_p, 1)
        r_p[i, :] = find_intersection_with_surface_eci(parameters.inc_range, r_s1[i], v_s1[i])
    end
    r_p_e = similar(r_p)
    for i in axes(r_p, 1)
        Q = SatelliteToolbox.r_eci_to_ecef(TEME(), PEF(), o_s1[i].t)
        r_p_e[i, :] =  (Q,) .* r_p[i, :]
    end
    r_p_e
end

function find_intersection_with_surface_ecef(parameters::Sim)
    orbp = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    asc_t = SARToolbox.ascend_descend(orbp, 0.001)
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    o_s1, r_s1, v_s1 = SARToolbox.propagate_with_mean_elements!(orbp, asc_t:1:asc_t+T)
    r_s1_e = similar(r_s1)
    v_s1_e = similar(v_s1)
    for i in eachindex(o_s1)
        epoch = o_s1[i].t
        sv = SatelliteToolbox.OrbitStateVector(epoch, r_s1[i], v_s1[i])
        sv = SatelliteToolbox.sv_eci_to_ecef(sv, SatelliteToolbox.TEME(), SatelliteToolbox.PEF(), epoch)
        r_s1_e[i] = sv.r
        v_s1_e[i] = sv.v
    end
    look_angle = parameters.inc_range
    b̂ = SARToolbox.wave_vector.(look_angle, 0)
    r_p = zeros(SVector{3,Float64}, (size(o_s1, 1), length(look_angle)))
    for i in eachindex(r_s1)
        Q_midpoint = @views SARToolbox.ECI_to_LVLH(r_s1_e[i], v_s1_e[i])
        Q_midpoint = transpose(Q_midpoint)
        b̂_ECEF = (Q_midpoint,) .* b̂
        r_p[i, :] = SARToolbox.los_to_surface.((r_s1_e[i],), b̂_ECEF)
    end
    r_p
end

function temporal_diff_first_eci(propagators, formation, inc_range, fr, t::AbstractVector, window_length::Int)
    orbp_s1, orbp_h1 = deepcopy(propagators[1]), deepcopy(propagators[2])
    o_s1, r_s1, v_s1 = SARToolbox.propagate_with_mean_elements!(orbp_s1, t)
    o_h1, r_h1, v_h1 = SARToolbox.propagate_with_mean_elements!(orbp_h1, t)
    r_h2 = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr = SARToolbox.relative_orb_to_Δr(formation, o_h1[i].a, u)
        r_h2[i] = SARToolbox.chief_to_deputy_r(r_h1[i], v_h1[i], Δr)
    end
    mid_index = ceil(Int, length(r_s1) / 2)
    r_p = find_intersection_with_surface_eci(inc_range, r_s1[mid_index], v_s1[mid_index])
    # Convert to ECEF to model rotation of Earth
    epoch_jd = map(x -> x.t, o_s1)
    r_s1_e = map((t, x) -> SatelliteToolbox.r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_s1)
    r_s1_e_mid = r_s1_e[mid_index]
    r_h1_e_mid = SatelliteToolbox.r_eci_to_ecef(TEME(), PEF(), epoch_jd[mid_index]) * r_h1[mid_index]
    r_h2_e = map((t, x) -> SatelliteToolbox.r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_h2)
    D = SatelliteToolbox.r_eci_to_ecef(TEME(), PEF(), epoch_jd[mid_index])
    r_p_e = (D,) .* r_p
    # r_s1_e = r_s1
    # r_h1_e = r_h1
    # r_h2_e = r_h2
    # r_s1_e_mid, r_h1_e_mid = r_s1_e[mid_index], r_h1_e[mid_index]
    # sh1, sh2 = (
    #     SARToolbox.compute_support(r_s1_e, r_s1_e, r_h1_e, r_p_e, fr),
    #     SARToolbox.compute_support(r_s1_e, r_s1_e, r_h2_e, r_p_e, fr),
    # )
    central_fr_index = ceil(Int, size(fr, 1) / 2)
    central_fr = fr[central_fr_index]
    Tullio.@tullio sh1[k] := SARToolbox.compute_support_new(r_s1_e_mid, r_s1_e_mid, r_h1_e_mid, r_p[k], central_fr)
    Tullio.@tullio sh2[i, j, k] := SARToolbox.compute_support_new(r_s1_e_mid, r_s1_e[i], r_h2_e[i], r_p[k], fr[j])
    # sh1 = reshape(sh1, (1, size(sh1)...))
    indices = SARToolbox.spectral_shift_loop(sh1, sh2, 1, window_length)
    dt = t[2] - t[1]
    [(window_length + 1 - i[1]) * dt for i in indices]
end

function run_temporal_shift_geometric(parameters::Sim; conventional_inc=true, los_scaling=true)
    orbp_s1 = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h1 = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp_s1) + parameters.ref_companion,
    )
    harmony_formation = parameters.chaser_companion
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp_s1))
    asc_t = SARToolbox.ascend_descend(orbp_h1, 0.001)
    t = asc_t:parameters.propagation_window_length:asc_t+T
    o_s1, r_s1, v_s1 = SARToolbox.propagate_with_mean_elements!(orbp_s1, t)
    o_h1, r_h1, v_h1 = SARToolbox.propagate_with_mean_elements!(orbp_h1, t)
    Δr = similar(r_h1)
    r_h2 = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr[i] = SARToolbox.relative_orb_to_Δr(harmony_formation, o_h1[i].a, u)
        r_h2[i] = SARToolbox.chief_to_deputy_r(r_h1[i], v_h1[i], Δr[i])
    end

    # Convert to ECEF to model rotation of Earth
    epoch_jd = map(x -> x.t, o_s1)
    svs = convert_to_ecef.(epoch_jd, r_s1, v_s1)
    r_s1_e = getfield.(svs, :r)
    v_s1_e = getfield.(svs, :v)
    svs = convert_to_ecef.(epoch_jd, r_h1, v_h1)
    r_h1_e = getfield.(svs, :r)
    v_h1_e = getfield.(svs, :v)
    svs = convert_to_ecef.(epoch_jd, r_h2, v_h1)
    r_h2_e = getfield.(svs, :r)
    r_p = zeros(SVector{3,Float64}, (size(r_s1, 1), size(parameters.inc_range, 1)))
    for i in axes(r_p, 1)
        r_p[i, :] = find_intersection_with_surface_eci(parameters.inc_range, r_s1_e[i], v_s1_e[i])
    end
    @tullio r_me_h1[i, j] := find_me(r_s1_e[i], r_h1_e[i], r_p[i, j])
    @tullio r_me_h2[i, j] := find_me(r_s1_e[i], r_h2_e[i], r_p[i, j])
    @tullio v_me_h1[i, j] := find_me_v(r_s1_e[i], r_h1_e[i], r_p[i, j], v_s1_e[i], v_h1_e[i])
    if los_scaling
        @tullio norm_bi_l[i, j] := norm(SARToolbox.unit_v(-r_s1_e[i] + r_p[i, j]) + SARToolbox.unit_v(-r_h1_e[i] + r_p[i, j]))
    else
        norm_bi_l = fill(2, size(r_me_h1))
    end
    l⃗_me = -r_me_h1 .+ r_p
    Δr_e = r_me_h2 - r_me_h1
    b̂_me = similar(r_p)
    Δr_lvlh = similar(Δr_e)
    for i in eachindex(b̂_me)
        Q_ECEF_to_LVLH = SARToolbox.ECI_to_LVLH(r_me_h1[i], v_me_h1[i])
        b̂_me[i] = Q_ECEF_to_LVLH * SARToolbox.unit_v(l⃗_me[i])
        Δr_lvlh[i] = Q_ECEF_to_LVLH * Δr_e[i]
    end
    ζ⃗ = SARToolbox.unit_v.(l⃗_me) .× (-v_me_h1 ./ norm.(r_me_h1))
    ζ̂ = SARToolbox.unit_v.(ζ⃗)
    at_baseline = zeros((size(r_s1, 1), size(parameters.inc_range, 1)))
    B_perp = zero(at_baseline)
    for col in axes(at_baseline, 2), row in axes(at_baseline, 1)
        Δr_r, Δr_t, Δr_n = Δr_lvlh[row, col][1], Δr_lvlh[row, col][2], Δr_lvlh[row, col][3]
        b_t_coreg = Δr_n * b̂_me[row, col][2] / b̂_me[row, col][3]
        at_baseline[row, col] = -Δr_t + b_t_coreg
        B_coregistered = [Δr_r, b_t_coreg, Δr_n]
        # B_perp[row, col] = norm(B_coregistered × b̂_me[row, col])
        Q_ECEF_to_LVLH = SARToolbox.ECI_to_LVLH(r_me_h1[row, col], v_me_h1[row, col])
        B_perp[row, col] = norm_bi_l[row, col] * (transpose(Q_ECEF_to_LVLH) * B_coregistered) ⋅ ζ̂[row, col]
    end
    λ₀ = SARToolbox.c₀ / SARToolbox.f₀
    n̂ = getindex.(SARToolbox.find_tangent_normal_basis.(r_p, l⃗_me), 3)
    if conventional_inc
        inc_angle = acos.(-SARToolbox.unit_v.(l⃗_me) .⋅ n̂)
    else
        inc_angle = asin.(ζ̂ .⋅ n̂)
    end
    Rₛ = LinearAlgebra.norm.(l⃗_me)
    @tullio sensitivity[i, j] := 2π * B_perp[i, j] / (λ₀ * Rₛ[i, j] * sin(inc_angle[i, j]))
    return (at_baseline ./ norm.(v_me_h1), at_baseline, sensitivity, B_perp)
end

function run_temporal_shift_drama_method(parameters::Sim)
    orbp_s1 = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h1 = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp_s1) + parameters.ref_companion,
    )
    orbp_me = SARToolbox.from_rel_orbit(
        Val(:J2),
        Propagators.mean_elements(orbp_s1),
        0,
        0,
        parameters.ref_companion.Δf / 2,
    )
    harmony_formation = parameters.chaser_companion
    inc_range = parameters.inc_range
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp_s1))
    asc_t = SARToolbox.ascend_descend(orbp_h1, 0.001)
    t = asc_t:parameters.propagation_window_length:asc_t+T
    o_s1, r_s1, v_s1 = SARToolbox.propagate_with_mean_elements!(orbp_s1, t)
    o_h1, r_h1, v_h1 = SARToolbox.propagate_with_mean_elements!(orbp_h1, t)
    _, r_me, v_me = SARToolbox.propagate_with_mean_elements!(orbp_me, t)
    Δr = similar(r_h1)
    r_h2 = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr[i] = SARToolbox.relative_orb_to_Δr(harmony_formation, o_h1[i].a, u)
        r_h2[i] = SARToolbox.chief_to_deputy_r(r_h1[i], v_h1[i], Δr[i])
    end

    # Convert to ECEF to model rotation of Earth
    epoch_jd = map(x -> x.t, o_s1)
    svs = convert_to_ecef.(epoch_jd, r_s1, v_s1)
    r_s1_e = getfield.(svs, :r)
    v_s1_e = getfield.(svs, :v)
    svs = convert_to_ecef.(epoch_jd, r_h1, v_h1)
    r_h1_e = getfield.(svs, :r)
    svs = convert_to_ecef.(epoch_jd, r_h2, v_h1)
    r_h2_e = getfield.(svs, :r)
    svs = convert_to_ecef.(epoch_jd, r_me, v_me)
    r_me_e = getfield.(svs, :r)
    v_me_e = getfield.(svs, :v)
    r_p = zeros(SVector{3,Float64}, (size(r_s1, 1), size(parameters.inc_range, 1)))
    for i in axes(r_p, 1)
        r_p[i, :] = find_intersection_with_surface_eci(parameters.inc_range, r_s1_e[i], v_s1_e[i])
    end
    Δr_e = r_h2_e - r_h1_e
    b̂_h1 = similar(r_p)
    b̂_s1 = similar(r_p)
    Δr_lvlh = similar(Δr_e)
    for i in axes(b̂_h1, 1)
        # LoS of the second satellite is from its antenna towards the
        # points on the ellipsoid
        b̂ = (-r_h1_e[i],) .+ r_p[i, :]
        Q_ECEF_to_LVLH = SARToolbox.ECI_to_LVLH(r_me_e[i], v_me_e[i])
        b̂_h1[i, :] = SARToolbox.unit_v.((Q_ECEF_to_LVLH,) .* b̂)
        b̂ = (-r_s1_e[i],) .+ r_p[i, :]
        b̂_s1[i, :] = SARToolbox.unit_v.((Q_ECEF_to_LVLH,) .* b̂)
        Δr_lvlh[i] = Q_ECEF_to_LVLH * Δr_e[i]
    end
    b̂_me = SARToolbox.unit_v.(b̂_s1 .+ b̂_h1)
    at_baseline = zeros((size(r_s1, 1), size(parameters.inc_range, 1)))
    B_perp = zero(at_baseline)
    for col in axes(at_baseline, 2), row in axes(at_baseline, 1)
        Δr_r, Δr_t, Δr_n = Δr_lvlh[row][1], Δr_lvlh[row][2], Δr_lvlh[row][3]
        b_t_coreg = Δr_n * b̂_me[row, col][2] / b̂_me[row, col][3]
        at_baseline[row, col] = -Δr_t + b_t_coreg
        B_coregistered = [Δr_r, b_t_coreg ./ 2, Δr_n]
        B_perp[row, col] = norm(B_coregistered × b̂_me[row, col])
    end
    λ₀ = SARToolbox.c₀ / SARToolbox.f₀
    inc_angle = SARToolbox.look_to_inc_angle.(inc_range, 693E3)
    @tullio Rₛ[i, j] := LinearAlgebra.norm(-r_h1_e[i] + r_p[i, j])
    @tullio sensitivity[i, j] := 2 * 2π * B_perp[i, j] / (λ₀ * Rₛ[i, j] * sin(inc_angle[j]))
    return (at_baseline, sensitivity, B_perp)
end

function run_temporal_shift_drama_method_surface(parameters::Sim)
    orbp_s1 = SARToolbox.create_reference_orbit(parameters.main_orb_tle)
    orbp_h1 = Propagators.init(
        Val(:J2),
        Propagators.mean_elements(orbp_s1) + parameters.ref_companion,
    )
    harmony_formation = parameters.chaser_companion
    inc_range = parameters.inc_range
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp_s1))
    asc_t = SARToolbox.ascend_descend(orbp_h1, 0.001)
    t = asc_t:parameters.propagation_window_length:asc_t+T
    o_s1, r_s1, v_s1 = SARToolbox.propagate_with_mean_elements!(orbp_s1, t)
    o_h1, r_h1, v_h1 = SARToolbox.propagate_with_mean_elements!(orbp_h1, t)
    Δr = similar(r_h1)
    r_h2 = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr[i] = SARToolbox.relative_orb_to_Δr(harmony_formation, o_h1[i].a, u)
        r_h2[i] = SARToolbox.chief_to_deputy_r(r_h1[i], v_h1[i], Δr[i])
    end

    # Convert to ECEF to model rotation of Earth
    epoch_jd = getfield.(o_s1, :t)
    svs = convert_to_ecef.(epoch_jd, r_s1, v_s1)
    r_s1_e = getfield.(svs, :r)
    v_s1_e = getfield.(svs, :v)
    svs = convert_to_ecef.(epoch_jd, r_h1, v_h1)
    r_h1_e = getfield.(svs, :r)
    svs = convert_to_ecef.(epoch_jd, r_h2, v_h1)
    r_h2_e = getfield.(svs, :r)
    r_p = zeros(SVector{3,Float64}, (size(r_s1, 1), size(parameters.inc_range, 1)))
    for i in axes(r_p, 1)
        r_p[i, :] = find_intersection_with_surface_eci(parameters.inc_range, r_s1_e[i], v_s1_e[i])
    end
    Δr_e = r_h2_e - r_h1_e
    b̂_h1 = similar(r_p)
    b̂_s1 = similar(r_p)
    Δr_lvlh = similar(r_p)
    b̂_ref = SARToolbox.wave_vector.(inc_range, 0)
    for i in axes(b̂_h1, 1)
        b̂ = (-r_h1_e[i],) .+ r_p[i, :]
        Q_EC_to_LTLN = SARToolbox.EC_to_tangent_normal.(r_p[i, :], b̂_ref)
        b̂_h1[i, :] .= SARToolbox.unit_v.(Q_EC_to_LTLN .* b̂)
        b̂ = (-r_s1_e[i],) .+ r_p[i, :]
        b̂_s1[i, :] .= SARToolbox.unit_v.(Q_EC_to_LTLN .* b̂)
        Δr_lvlh[i, :] .= Q_EC_to_LTLN .* (Δr_e[i],)
    end
    b̂_me = SARToolbox.unit_v.(b̂_s1 .+ b̂_h1)
    at_baseline = zeros((size(r_s1, 1), size(parameters.inc_range, 1)))
    B_perp = zero(at_baseline)
    for col in axes(at_baseline, 2), row in axes(at_baseline, 1)
        Δr_r, Δr_t, Δr_n = Δr_lvlh[row, col][1], Δr_lvlh[row, col][3], Δr_lvlh[row, col][2]
        b_t_coreg = Δr_n * b̂_me[row, col][3] / b̂_me[row, col][2]
        at_baseline[row, col] = -Δr_t + b_t_coreg
        B_coregistered = [Δr_r, b_t_coreg, Δr_n]
        B_perp[row, col] = norm(B_coregistered × b̂_me[row, col])
    end
    λ₀ = SARToolbox.c₀ / SARToolbox.f₀
    inc_angle = SARToolbox.look_to_inc_angle.(inc_range, 693E3)
    @tullio Rₛ[i, j] := LinearAlgebra.norm(-r_h1_e[i] + r_p[i, j])
    @tullio sensitivity[i, j] := 2π * B_perp[i, j] / (λ₀ * Rₛ[i, j] * sin(inc_angle[j]))
    return (at_baseline, sensitivity, B_perp)
end

function convert_to_ecef(t, r, v)
    sv = SatelliteToolbox.OrbitStateVector(t, r, v)
    sv = SatelliteToolbox.sv_eci_to_ecef(sv, SatelliteToolbox.TEME(), SatelliteToolbox.PEF(), t)
    return sv
end

function find_me(r_t, r_r, r_p)
    lₘ = SARToolbox.unit_v(-r_p + r_r) + SARToolbox.unit_v(-r_p + r_t)
    lᵣ = r_r - r_t
    s, _ = [lₘ -lᵣ] \ (r_t - r_p)
    r_p + s * lₘ
end

function find_me_v(r_t, r_r, r_p, v_t, v_r)
    lᵣ = SARToolbox.unit_v(-r_p + r_r)
    lₜ = SARToolbox.unit_v(-r_p + r_t)
    lₘ = lᵣ + lₜ
    Δr = r_r - r_t
    _, t = [lₘ -Δr] \ (r_t - r_p)
    v_t + (v_r - v_t) * t
end
