function get_orbits_from_nc(fn::String, fraction_of_orbit=1)
    println("💾 Reading data from NetCDF.")
    time = NetCDF.ncread(fn, "t")
    if fraction_of_orbit ≈ 1
        count = [-1,]
    else
        last_time_index = round(Int, length(time) * fraction_of_orbit)
        count = [last_time_index,]
        time = time[1:last_time_index]
    end
    r_s1 = read_vec3(fn, "r_s1"; count)
    r_h1 = read_vec3(fn, "r_h1"; count)
    v_s1 = read_vec3(fn, "v_s1"; count)
    v_h1 = read_vec3(fn, "v_h1"; count)
    r_p = read_vec3(fn, "r_p"; count=[-1, count...])
    lats_s1 = NetCDF.ncread(fn, "lats_s1"; count=count)
    epochs = NetCDF.ncread(fn, "epoch"; count=count) # in seconds
    epochs = Dates.Millisecond.(round.(Int, epochs * 1E3)) # convert to ms
    ref_epoch = NetCDF.ncgetatt(fn, "epoch", "reference")
    epochs = Dates.DateTime(ref_epoch) .+ epochs
    epochs = SatelliteToolbox.date_to_jd.(epochs) # convert to julian day
    inc_range = NetCDF.ncread(fn, "inc"; count=[-1,])

    if (size(r_p, 2) == size(r_s1, 1)) && (size(r_p, 1) == size(inc_range, 1))
        r_p = permutedims(r_p)
    end
    a = NetCDF.ncread(fn, "a")
    e = NetCDF.ncread(fn, "e")
    i = NetCDF.ncread(fn, "i")
    raan = NetCDF.ncread(fn, "raan")
    arg_p = NetCDF.ncread(fn, "arg_p")
    true_anomaly = NetCDF.ncread(fn, "true_anomaly")
    println("🛰 Constructing orbit structure.")
    o_h1 = map(
        (t, a, e, i, raan, arg_p, true_anomaly) ->
            SatelliteToolbox.Orbit(t, a, e, i, raan, arg_p, true_anomaly),
        epochs,
        a,
        e,
        i,
        raan,
        arg_p,
        true_anomaly,
    )
    println("✅ Finished reading data from NetCDF.")
    return (r_s1, v_s1, o_h1, r_h1, v_h1, lats_s1, time, r_p, inc_range)
end

function ec_to_lvlh(r, v)
    r = SARToolbox.unit_v(r)
    v = SARToolbox.unit_v(v)
    n = v × r
    [v'; r'; n']
end
